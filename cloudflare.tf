# For Account ID
data "cloudflare_accounts" "aungmyatkyaw" {
  name = "aungmyatkyaw"
}

# zone
data "cloudflare_zone" "aungmyatkyaw_site" {
  account_id = data.cloudflare_accounts.aungmyatkyaw.accounts.0.id
  name       = var.cloudflare_website_name
}

resource "cloudflare_record" "sonarqube" {
  name            = "sonarqube"
  proxied         = true
  ttl             = 1
  type            = "A"
  allow_overwrite = true
  value           = aws_eip.devsecops_ip.public_ip
  zone_id         = data.cloudflare_zone.aungmyatkyaw_site.id
}

resource "cloudflare_record" "dev_frontend" {
  name            = "dev-frontend"
  proxied         = true
  ttl             = 1
  type            = "A"
  allow_overwrite = true
  value           = aws_eip.devsecops_ip.public_ip
  zone_id         = data.cloudflare_zone.aungmyatkyaw_site.id
}

resource "cloudflare_record" "qa_frontend" {
  name            = "qa-frontend"
  proxied         = true
  ttl             = 1
  type            = "A"
  allow_overwrite = true
  value           = aws_eip.devsecops_ip.public_ip
  zone_id         = data.cloudflare_zone.aungmyatkyaw_site.id
}

resource "cloudflare_record" "uat_frontend" {
  name            = "uat-frontend"
  proxied         = true
  ttl             = 1
  type            = "A"
  allow_overwrite = true
  value           = aws_eip.devsecops_ip.public_ip
  zone_id         = data.cloudflare_zone.aungmyatkyaw_site.id
}

resource "cloudflare_record" "prod_frontend" {
  name            = "frontend"
  proxied         = true
  ttl             = 1
  type            = "A"
  allow_overwrite = true
  value           = aws_eip.devsecops_ip.public_ip
  zone_id         = data.cloudflare_zone.aungmyatkyaw_site.id
}

resource "cloudflare_record" "mysql" {
  name            = "mysql"
  proxied         = true
  ttl             = 1
  type            = "A"
  allow_overwrite = true
  value           = aws_eip.devsecops_ip.public_ip
  zone_id         = data.cloudflare_zone.aungmyatkyaw_site.id
}
