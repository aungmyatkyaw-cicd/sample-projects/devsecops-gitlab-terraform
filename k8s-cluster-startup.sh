#!/bin/bash

export TOKEN=`/usr/bin/curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`

export INSTANCE_ID=`/usr/bin/curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/instance-id`

export REGION=`/usr/bin/curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/placement/region`

export TAG=`/usr/bin/curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/tags/instance/Name`

export ENV=`/usr/bin/curl -H "X-aws-ec2-metadata-token: $TOKEN" -s http://169.254.169.254/latest/meta-data/tags/instance/Env`

export ASSOCIATED_ID=`aws ec2 describe-addresses --output text --region $REGION --query 'Addresses[*].InstanceId' --filters Name="tag:Name",Values="$TAG" --filters Name="tag:Env",Values="$ENV"`

export IP_ADDR=`aws ec2 describe-addresses --output text --region $REGION --query 'Addresses[*].PublicIp' --filters Name="tag:Name",Values="$TAG" --filters Name="tag:Env",Values="$ENV"`

echo $IP_ADDR

echo ".........----------------#################._.-.-EIP_CHANGE-.-._.#################----------------........."
if [ "$INSTANCE_ID" != "$ASSOCIATED_ID" ]
then
    # Retrieve the Elastic IP using the meta-data
    export EID=$(echo $(aws ec2 describe-addresses --output text --region $REGION --query 'Addresses[*].AllocationId' --filters Name="tag:Name",Values="$TAG" --filters Name="tag:Env",Values="$ENV") | cut --delimiter " " --fields 1)

    # Check the IP and associate with current instance
    [ ! -z "$EID" ] && aws ec2 associate-address --instance-id $INSTANCE_ID --allocation-id $EID --allow-reassociation
fi
echo ".........----------------#################._.-.-COMPLETED-.-._.#################----------------........."

# export EXISTING_ADDR=`grep '^\s*- --advertise-address=' /etc/kubernetes/manifests/kube-apiserver.yaml | awk -F'=' '{print $2}' | awk '{print $1}'`

# echo $EXISTING_ADDR

# find /etc/kubernetes -type f -exec sed -i "s/${EXISTING_ADDR}/${IP_ADDR}/g" {} +

# Stop Services
systemctl stop kubelet docker containerd

rm -rf /etc/kubernetes-backup
rm -rf /var/lib/kubelet-backup

# Backup Kubernetes and kubelet
mv -f /etc/kubernetes /etc/kubernetes-backup
mv -f /var/lib/kubelet /var/lib/kubelet-backup

# Keep the certs we need
mkdir -p /etc/kubernetes
cp -r /etc/kubernetes-backup/pki /etc/kubernetes
rm -rf /etc/kubernetes/pki/{apiserver.*,etcd/peer.*}

# Start docker
systemctl start docker containerd

# Init cluster with new ip address
kubeadm init --control-plane-endpoint $IP_ADDR --ignore-preflight-errors=DirAvailable--var-lib-etcd

mkdir -p ~/.kube
cp -rf /etc/kubernetes/admin.conf ~/.kube/config
bash

export KUBECONFIG=/etc/kubernetes/admin.conf

# setup for ubuntu user
mkdir -p /home/ubuntu/.kube
cp -rf /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
chown -R ubuntu:ubuntu /home/ubuntu/.kube

echo "untaint controlplane node"
node=$(kubectl get nodes -o=jsonpath='{.items[*].metadata.name}')
kubectl taint node $node node-role.kubernetes.io/control-plane:NoSchedule-
kubectl get nodes -o wide

sleep 60

kubectl delete node $(kubectl get nodes | grep -i NotReady | awk '{print $1;}')
