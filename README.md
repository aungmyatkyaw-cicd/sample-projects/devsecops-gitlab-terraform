# Terraform Project: DevSecOps Testing Environment

Welcome to the Terraform project repository for setting up a DevSecOps testing environment on AWS, Cloudflare, and GitLab.

## Overview

This Terraform project automates the provisioning of infrastructure components required for a DevSecOps testing environment. It includes deploying an EC2 instance for a single-node Kubernetes cluster, setting up a spot fleet request, CloudWatch monitoring, and integrating with Cloudflare for DNS management.

### Key Components

- **EC2 Instance (Kubernetes Node)**:
  - Provisioned for a single-node Kubernetes cluster.
  - Configured with necessary tools in startup scripts for Kubernetes applications.

- **Spot Fleet Request**:
  - Manages spot instances to optimize cost and availability.
  - Scales based on workload demands using spot instances.

- **CloudWatch Monitoring**:
  - Monitors EC2 instance health and performance metrics.
  - Alarms trigger notifications for critical events or breaches.

- **AWS, Cloudflare, and GitLab Integration**:
  - Terraform provisions AWS resources.
  - Cloudflare manages DNS and routing.
  - GitLab integrates for DevSecOps pipeline testing with remote CI/CD template.

## Usage

### Pre-requisites

- Install Terraform
- Configure AWS CLI credentials

### Deployment Steps

1. **Clone Repository**:

   ```bash
   git clone https://github.com/your-account/devsecops-gitlab-terraform.git
   cd devsecops-gitlab-terraform

2. **Initialize Terraform**:

    ```bash
   terraform init

3. **Review and Modify Configuration**:

- Customize variables.tf for Cloudflare and GitLab credentials, region, etc.
- Modify main.tf for resource settings with AWS credentials.

4. **Deploy Infrastructure**:

    ```bash
   terraform apply

## Support
For support or assistance, please reach out via the issue tracker on the repository.
