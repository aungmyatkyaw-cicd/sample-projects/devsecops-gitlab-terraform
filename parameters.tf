resource "aws_ssm_parameter" "frontend_deploy_variables" {
  provider = aws.mumbai
  for_each = var.frontend_deploy_variables
  name     = "/${data.gitlab_project.frontend_sample_project.id}/${data.gitlab_project.frontend_sample_project.path}/${each.key}"
  type     = "SecureString"
  value    = each.value
}

resource "aws_ssm_parameter" "springboot_deploy_variables" {
  provider = aws.mumbai
  for_each = var.springboot_deploy_variables
  name     = "/${data.gitlab_project.springboot_sample_project.id}/${data.gitlab_project.springboot_sample_project.path}/${each.key}"
  type     = "SecureString"
  value    = each.value
}

resource "aws_ssm_parameter" "dotnet_deploy_variables" {
  provider = aws.mumbai
  for_each = var.dotnet_deploy_variables
  name     = "/${data.gitlab_project.dotnet_sample_project.id}/${data.gitlab_project.dotnet_sample_project.path}/${each.key}"
  type     = "SecureString"
  value    = each.value
}
