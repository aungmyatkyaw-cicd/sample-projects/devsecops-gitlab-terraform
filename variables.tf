variable "main_cidr_block" {
  description = "Main CIDR Block for All Regions"
  default     = "10.0.0.0/8"
}

variable "aws_region_list_for_cidr" {
  description = "AWS Region List for creating CIDR Blocks"
  default = {
    "ap-southeast-1" = 0
    "ap-south-1"     = 1
    "us-west-2"      = 2
    "ap-south-2"     = 3
  }
}

variable "server_tag_value" {
  description = "Tag Value for server"
  default     = "devsecops-gitlab"
}

variable "environment" {
  description = "Env Value for server"
  default     = "dev"
}

variable "staging_ec2_key_name" {
  description = "Staging Server EC2 Key Name"
  default     = "devsecops-gitlab"
}

variable "configs_bucket_name" {
  description = "Storage Bucket for Config files"
  default     = "config-files-amk-152"
}

variable "staging_spot_instance_types" {
  description = "Spot Instance Types for staging server"
  type        = list(string)
  default     = ["t3a.xlarge"]
}

variable "machine_type" {
  description = "Machine Architecture"
  default     = "amd64"
}

variable "subscription_emails" {
  description = "Create Topic Subscriptions with these emails for current region"
  type        = list(string)
  default     = ["aungmyatkyaw.kk@gmail.com"]
}

variable "istio_ingress_gateway_nodeport" {
  description = "NodePort Number for Istio Ingress Gateway"
  type        = number
  default     = 30808
}

variable "cloudflare_api_token" {
  description = "API Token for cloudflare"
  type        = string
}

variable "cloudflare_website_name" {
  description = "Website name"
  type        = string
  default     = "aungmyatkyaw.site"
}

variable "gitlab_token" {
  description = "API Token for Gitlab"
  type        = string
}

variable "frontend_gitlab_path" {
  description = "Frontend Sample Project Path on Gitlab"
  type        = string
  default     = "aungmyatkyaw-cicd/sample-projects/angular-frontend-example"
}

variable "frontend_deploy_variables" {
  type = map(string)
  default = {
    "dev/istio.gateways"  = "istio-system/amk-gateway"
    "dev/istio.host.url"  = "dev-frontend.aungmyatkyaw.site"
    "qa/istio.gateways"   = "istio-system/amk-gateway"
    "qa/istio.host.url"   = "qa-frontend.aungmyatkyaw.site"
    "uat/istio.gateways"  = "istio-system/amk-gateway"
    "uat/istio.host.url"  = "uat-frontend.aungmyatkyaw.site"
    "prod/istio.gateways" = "istio-system/amk-gateway"
    "prod/istio.host.url" = "frontend.aungmyatkyaw.site"
  }
}

variable "springboot_gitlab_path" {
  description = "Springboot Sample Project Path on Gitlab"
  type        = string
  default     = "aungmyatkyaw-cicd/sample-projects/spring-boot-3-rest-api-example"
}

variable "springboot_deploy_variables" {
  type = map(string)
  default = {
    "dev/server.port"  = "8080"
    "qa/server.port"   = "8080"
    "uat/server.port"  = "8080"
    "prod/server.port" = "8080"
  }
}

variable "dotnet_gitlab_path" {
  description = "Dotnet Sample Project Path on Gitlab"
  type        = string
  default     = "aungmyatkyaw-cicd/sample-projects/dotnet-6-rest-api-example"
}

variable "dotnet_deploy_variables" {
  type = map(string)
  default = {
    "dev/env.MY_APP_MysqlDbSettings__Host"      = "mysql.db.svc.cluster.local"
    "dev/env.MY_APP_MysqlDbSettings__User"      = "admin"
    "dev/env.MY_APP_MysqlDbSettings__Password"  = "abcdefgh"
    "qa/env.MY_APP_MysqlDbSettings__Host"       = "mysql.db.svc.cluster.local"
    "qa/env.MY_APP_MysqlDbSettings__User"       = "admin"
    "qa/env.MY_APP_MysqlDbSettings__Password"   = "abcdefgh"
    "uat/env.MY_APP_MysqlDbSettings__Host"      = "mysql.db.svc.cluster.local"
    "uat/env.MY_APP_MysqlDbSettings__User"      = "admin"
    "uat/env.MY_APP_MysqlDbSettings__Password"  = "abcdefgh"
    "prod/env.MY_APP_MysqlDbSettings__Host"     = "mysql.db.svc.cluster.local"
    "prod/env.MY_APP_MysqlDbSettings__User"     = "admin"
    "prod/env.MY_APP_MysqlDbSettings__Password" = "abcdefgh"
  }
}
