// Store Terraform Backend State on S3 Bucket
terraform {
  backend "s3" {
    bucket         = "terraform-backend-state-amk-152"
    key            = "devsecops-gitlab/backend-state"
    region         = "ap-southeast-1"
    dynamodb_table = "terraform_state_locks"
    encrypt        = true
    profile        = "amk"
  }
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

// Define Provider and Region
provider "aws" {
  region  = "ap-south-1"
  profile = "amk"
  alias   = "mumbai"
  default_tags {
    tags = {
      Project = "DevSecOps-Gitlab"
    }
  }
}

// Define Provider and Region
provider "aws" {
  region  = "ap-southeast-1"
  profile = "amk"
  default_tags {
    tags = {
      Project = "DevSecOps-Gitlab"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

# Configure the GitLab Provider
provider "gitlab" {
  token = var.gitlab_token
}

