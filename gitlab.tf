data "gitlab_project" "frontend_sample_project" {
  path_with_namespace = var.frontend_gitlab_path
}

data "gitlab_project" "springboot_sample_project" {
  path_with_namespace = var.springboot_gitlab_path
}

data "gitlab_project" "dotnet_sample_project" {
  path_with_namespace = var.dotnet_gitlab_path
}

